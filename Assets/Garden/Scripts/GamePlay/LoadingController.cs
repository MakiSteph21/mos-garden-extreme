﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingController : MonoBehaviour
{
    [SerializeField]
    private Image ProgressBar;
    [SerializeField]
    private GameObject Selection;
    [SerializeField]
    private MenuManager MenuManager;

    private void Start()
    {
        StartCoroutine(InitializeGame());
    }

    IEnumerator InitializeGame()
    {
        float percentage = 0.0f;
        float MAX_TIME = 100.0f;
        for (; percentage <= MAX_TIME; percentage++)
        {
            float progress = percentage / MAX_TIME;
            ProgressBar.fillAmount = progress;
            yield return new WaitForSeconds(.10f);
        }
        MenuManager.GoToMenu(Selection);
    }
}
