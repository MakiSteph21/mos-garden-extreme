﻿namespace Common.Task.Model
{
	public enum TaskType
	{
		Assetbundle,
		Texture,
		CacheableTexture,
	}

	public enum NetworkTaskType
	{
		None,
		JSON
	}

	public enum DownloadTaskType
	{
		Default,
		Assetbundle,
		JSON
	}
}

