using UnityEngine;

using Common.Utils;

namespace Common.Timer {
    public class CountdownTimer {
        private float m_PolledTime;
        private float m_CountdownTime;

        private TimeReference m_TimeReference;
    
        public CountdownTimer(float countdownTime, TimeReference timeReference) {
            this.m_TimeReference = timeReference;

            if(countdownTime < 0)
                Debug.LogWarning("The specified time must be greater than zero.");

            Reset(countdownTime);
        }

        public CountdownTimer(float countdownTime, string timeReferenceName) : this(countdownTime, TimeReferencePool.GetInstance().Get(timeReferenceName)) {}

        public CountdownTimer(float countdownTime) : this(countdownTime, TimeReference.GetDefaultInstance()) {}

        public void Update() {
            this.m_PolledTime += this.m_TimeReference.DeltaTime;
        }
       
        public void Reset() {
            this.m_PolledTime = 0;
        }

        public void Reset(float countdownTime) {
            Reset();
            this.m_CountdownTime = countdownTime;
        }

        public bool HasElapsed() {
            return Comparison.TolerantGreaterThanOrEquals(this.m_PolledTime, this.m_CountdownTime);
        }

        public float GetRatio() {
            float ratio = this.m_PolledTime / this.m_CountdownTime;
            return Mathf.Clamp(ratio, 0, 1);
        }

        public float GetPolledTime() {
            return this.m_PolledTime;
        }

        public void SetPolledTime(float polledTime) {
            this.m_PolledTime = polledTime;
        }

        private int timeLeft = 0;
        public void EndTimer() {
            
            timeLeft = (int)this.m_CountdownTime - (int)this.m_PolledTime;
            this.m_PolledTime = this.m_CountdownTime + this.m_PolledTime;
        }

         public string Decrement() {
            timeLeft--;
            if (timeLeft < 0)
                timeLeft = 0;
            return string.Format("0:{0:d2}", timeLeft);
        }


        public void SetCountdownTime(float newTime) {
            this.m_CountdownTime = newTime;
        }

        public float GetCountdownTime() {
            return this.m_CountdownTime;
        }

        public override string ToString() {
            float timeRemaining = m_CountdownTime - m_PolledTime;
            int minutes = (int)(timeRemaining / 60.0f);
            int seconds = ((int)timeRemaining) % 60;
            return string.Format("{0}:{1:d2}", minutes, seconds);
        }
    }
}