using System;
using System.Collections.Generic;

namespace Common.Timer {
    public class TimeReferencePool {
        private IDictionary<string, TimeReference> m_InstanceMap;

        private TimeReferencePool() {
            m_InstanceMap = new Dictionary<string, TimeReference>();
        }

        private static TimeReferencePool m_OnlyInstance = null;

        public static TimeReferencePool GetInstance() {
            if(m_OnlyInstance == null)
                m_OnlyInstance = new TimeReferencePool();
            return m_OnlyInstance;
        }

        public TimeReference Add(string name) {
            TimeReference newTimeReference = new TimeReference(name);
            m_InstanceMap[name] = newTimeReference;
            return newTimeReference;
        }

        public TimeReference Get(string name) {
            if(string.IsNullOrEmpty(name))
                return TimeReference.GetDefaultInstance();

            return m_InstanceMap[name];
        }

        public TimeReference GetDefault() {
            return TimeReference.GetDefaultInstance();
        }
    }
}