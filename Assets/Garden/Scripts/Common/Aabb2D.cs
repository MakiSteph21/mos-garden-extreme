using UnityEngine;
using System.Collections.Generic;

using Common.Utils;

namespace Common.Shapes
{
    public class Aabb2D
    {
        private Vector2 Min;
        private Vector2 Max;
        private const float BIG_NUMBER = 1e37f;

        public Aabb2D()
        {
            this.Min = new Vector2();
            this.Max = new Vector2();
            Empty();
        }

        public Aabb2D(Vector2 v1, Vector2 v2)
        {
            this.Min = new Vector2();
            this.Max = new Vector2();
            Empty();

            AddToContain(v1);
            AddToContain(v2);
        }

        public Aabb2D(Rect rect)
        {
            this.Min = new Vector2();
            this.Max = new Vector2();

            Empty();

            AddToContain(new Vector2(rect.xMin, rect.yMin));
            AddToContain(new Vector2(rect.xMax, rect.yMax));
        }

        public void Empty()
        {
            this.Min.x = BIG_NUMBER;
            this.Min.y = BIG_NUMBER;

            this.Max.x = -BIG_NUMBER;
            this.Max.y = -BIG_NUMBER;
        }

        public bool IsEmpty()
        {
            return (this.Min.x > this.Max.x) && (this.Min.y > this.Max.y);
        }

        public Vector2 GetMinimum()
        {
            return this.Min;
        }

        public Vector2 GetMaximum()
        {
            return this.Max;
        }

        public Vector2 GetCenter()
        {
            return (this.Min + this.Max) * 0.5f;
        }

        public Vector2 GetSize()
        {
            return this.Max - this.Min;
        }

        public Vector2 GetRadiusVector()
        {
            return GetSize() * 0.5f;
        }

        public float GetRadius()
        {
            return GetRadiusVector().magnitude;
        }

        public void AddToContain(Vector2 v)
        {
            // expand Min
            if (v.x < Min.x)
            {
                Min.x = v.x;
            }

            if (v.y < Min.y)
            {
                Min.y = v.y;
            }

            // expand Max
            if (v.x > Max.x)
            {
                Max.x = v.x;
            }

            if (v.y > Max.y)
            {
                Max.y = v.y;
            }
        }

        public bool Contains(Vector2 v)
        {

            return (Comparison.TolerantLesserThanOrEquals(Min.x, v.x) && Comparison.TolerantLesserThanOrEquals(v.x, Max.x))
                && (Comparison.TolerantLesserThanOrEquals(Min.y, v.y) && Comparison.TolerantLesserThanOrEquals(v.y, Max.y));
        }

        public bool IsOverlapping(Aabb2D otherBox)
        {
            // get all corners
            // return true if there is at least one corner that is contained within the bounding box
            return Contains(otherBox.GetTopLeft())
                || Contains(otherBox.GetBottomLeft())
                || Contains(otherBox.GetTopRight())
                || Contains(otherBox.GetBottomRight());
        }

        public void Translate(Vector2 translation)
        {
            if (IsEmpty())
            {
                // no need to translate if it is empty
                return;
            }

            // transform to local space
            Vector2 center = GetCenter();
            this.Min -= center;
            this.Max -= center;

            // translate
            this.Min += translation;
            this.Max += translation;
        }

        public Aabb2D GetAabbInLocalSpace()
        {
            Aabb2D localAabb = new Aabb2D();

            Vector2 center = GetCenter();
            localAabb.AddToContain(this.Min - center);
            localAabb.AddToContain(this.Max - center);

            return localAabb;
        }

        public Vector2 GetTopLeft()
        {
            return new Vector2(Min.x, Max.y);
        }

        public Vector2 GetBottomLeft()
        {
            return this.Min;
        }

        public Vector2 GetTopRight()
        {
            return this.Max;
        }

        public Vector2 GetBottomRight()
        {
            return new Vector2(Max.x, Min.y);
        }

        public override string ToString()
        {
            return "Min: " + Min + "; Max: " + Max;
        }
    }
}