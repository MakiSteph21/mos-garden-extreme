﻿using System;
using UnityEngine;
using Common.Timer;
using Common.Utils;

namespace Common.AI.Fsm.Action
{
	public class MoveAlongDirectionByPolledTime : FsmActionAdapter
	{
		private Transform actor;
		private Vector3 direction;
		private float velocity;

		private TimeReference timeReference;
		private float polledtime;
		private float duration;

		private string finishEvent;

		private Vector3 startPosition;

		private CountdownTimer timer;

		public MoveAlongDirectionByPolledTime (FsmState owner, string timeReferenceName) : base (owner)
		{
			this.timeReference = TimeReferencePool.GetInstance ().Get (timeReferenceName);
			this.timer = new CountdownTimer(1);
		}

		public void Init (Transform actor, Vector3 direction, float velocity, float duration, string finishEvent)
		{
			this.actor = actor;
			this.direction = direction;
			this.velocity = velocity;
			this.duration = duration;
		}

		public Vector3 Startposition {
			get {
				return startPosition;
			}
			set {
				startPosition = value;
			}
		}

		public override void OnEnter ()
		{
			if (Comparison.TolerantEquals (direction.sqrMagnitude, 0)) {
				Debug.LogError ("Direction cannot be zero.");
			}

			if (!Comparison.TolerantEquals (this.direction.sqrMagnitude, 1)) {
				this.direction.Normalize ();
			}

			timer.Reset(this.duration);
		}

		public override void OnUpdate ()
		{
			timer.Update();

			if(timer.HasElapsed())
            {
                Finish();
                return;
            }

			this.polledtime += this.timeReference.DeltaTime;
			Vector3 newPosition = this.startPosition + (this.direction * (this.velocity * this.polledtime));
			this.actor.position = newPosition;

			
		}

		private void Finish() {
			if(!string.IsNullOrEmpty(finishEvent)) {
                GetOwner().SendEvent(finishEvent);
            }
		}
	}
}