using System;
using System.Collections;
using System.Collections.Generic;

public class InfoEventArgs<T> {
    public T Info;
    public InfoEventArgs(T info = default(T)) {
        this.Info = info;
    }
}