using UnityEngine;
using System.Collections;

namespace Common.Animations {
    public class RectTransformScaleTweener : Vector3Tweener {
        RectTransform RectTransform;

        protected override void OnUpdate() {
            base.OnUpdate();
            RectTransform.localScale = CurrentTweenValue;
        }
    }
}