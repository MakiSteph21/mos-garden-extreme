﻿using UnityEngine;
using System;
using System.Collections;

namespace Common.Animations {
	public static class RectTransformAnimationExtensions 
	{
		public static Tweener AnchorTo (this RectTransform t, Vector3 position)
		{
			return AnchorTo (t, position, Tweener.DefaultDuration);
		}
		
		public static Tweener AnchorTo (this RectTransform t, Vector3 position, float duration)
		{
			return AnchorTo (t, position, duration, Tweener.DefaultEquation);
		}
		
		public static Tweener AnchorTo (this RectTransform t, Vector3 position, float duration, Func<float, float, float, float> equation)
		{
			RectTransformAnchorPositionTweener tweener = t.gameObject.AddComponent<RectTransformAnchorPositionTweener> ();
			tweener.StartTweenValue = t.anchoredPosition;
			tweener.EndTweenValue = position;
			tweener.duration = duration;
			tweener.equation = equation;
			tweener.Play ("UNPAUSABLE");
			return tweener;
		}

		public static Tweener ScaleTo (this RectTransform t, Vector3 scale)
		{
			return ScaleTo (t, scale, Tweener.DefaultDuration);
		}
		
		public static Tweener ScaleTo (this RectTransform t, Vector3 scale, float duration)
		{
			return ScaleTo (t, scale, duration, Tweener.DefaultEquation);
		}

		public static Tweener ScaleTo(this RectTransform t, Vector3 scale, float duration, Func<float, float, float, float> equation) {
			RectTransformScaleTweener tweener = t.gameObject.AddComponent<RectTransformScaleTweener>();
			tweener.StartTweenValue = t.localScale;
			tweener.EndTweenValue = scale;
			tweener.duration = duration;
			tweener.equation = equation;
			tweener.Play ("UNPAUSABLE");
			return tweener;
		}
	}
}