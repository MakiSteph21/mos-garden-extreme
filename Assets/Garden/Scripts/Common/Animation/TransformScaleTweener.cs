﻿using UnityEngine;
using System.Collections;

namespace Common.Animations {
	public class TransformScaleTweener : Vector3Tweener 
	{
		protected override void OnUpdate ()
		{
			base.OnUpdate ();
			transform.localScale = CurrentTweenValue;
		}
	}
}