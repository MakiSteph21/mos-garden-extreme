﻿using UnityEngine;
using System.Collections;

namespace Common.Animations {
	public class TransformLocalPositionTweener : Vector3Tweener 
	{
		protected override void OnUpdate ()
		{
			base.OnUpdate ();
			transform.localPosition = CurrentTweenValue;
		}
	}
}