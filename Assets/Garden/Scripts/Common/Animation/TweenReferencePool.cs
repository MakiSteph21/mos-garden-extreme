using System;
using System.Collections.Generic;

namespace Common.Animations {
    public class TweenReferencePool {
        private IDictionary<string, List<EasingControl>> m_InstanceMap;
        public bool OnPause = false;

        private TweenReferencePool() {
            m_InstanceMap = new Dictionary<string, List<EasingControl>>();
        }

        private static TweenReferencePool m_OnlyInstance = null;

        public static TweenReferencePool GetInstance() {
            if(m_OnlyInstance == null)
                m_OnlyInstance = new TweenReferencePool();
            return m_OnlyInstance;
        }

        public void Add(string name, EasingControl easingControl) {
            if(name.Equals("PAUSABLE") && OnPause)
                easingControl.Pause();            

            if(m_InstanceMap.ContainsKey(name))
                m_InstanceMap[name].Add(easingControl);
            else {
                m_InstanceMap[name] = new List<EasingControl>() { easingControl };
            }
        }

        public void Remove(string name, EasingControl easingControl) {
            if(m_InstanceMap.ContainsKey(name))
                m_InstanceMap[name].Remove(easingControl);
        }

        public List<EasingControl> GetList(string name) {
            if(m_InstanceMap.ContainsKey(name))
                return m_InstanceMap[name];
            return null;
        }
    }
}