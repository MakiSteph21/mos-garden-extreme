namespace Common.Collections.Generic {
    public struct StringEntry<TValue> {
        public string Key;
        public TValue Value;
        public StringEntry(string key, TValue value) {
            Key = key;
            Value = value;
        }
    }
}