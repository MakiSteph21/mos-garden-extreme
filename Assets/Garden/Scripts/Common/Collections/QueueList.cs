using System;
using System.Collections;
using System.Collections.Generic;

namespace Common.Collections.Generic {
    public class QueueList<T> {
        private List<T> m_List;

        public int Count {
            get {
                return m_List.Count;
            }
        }

        public QueueList() {
            m_List = new List<T>();
        }

        public QueueList(List<T> list) {
            m_List = list;
        }

        public void Enqueue(T obj) {
            m_List.Add(obj);
        }

        public T Dequeue() {
            T obj = m_List[Count - 1];
            m_List.Remove(obj);
            return obj;
        }

        public T Peek() {
            return m_List[Count - 1];
        }

        public override string ToString() {
            return m_List.JSerialize();
        }
    }
}