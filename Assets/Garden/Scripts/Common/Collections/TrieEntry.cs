using System.Collections.Generic;

namespace Common.Collections.Generic {
    public struct TrieEntry<TKey, TValue> {
        public IEnumerable<TKey> Key;
        public TValue Value;
        public TrieEntry(IEnumerable<TKey> key, TValue value) {
            Key = key;
            Value = value;
        }
    }
}