using UnityEngine;

public static class JSerializeUtility {
    public static string JSerialize(this object obj) {
        return JsonUtility.ToJson(obj);
    }

    public static T JDeserialize<T>(this string json) {
        return JsonUtility.FromJson<T>(json);
    }
}