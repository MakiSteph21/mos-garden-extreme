using System;
using UnityEngine;

namespace Common.Utils {
    public static class VectorUtils {
        public static bool Equals(this Vector3 toCompare, Vector3 other) {
            return Comparison.TolerantEquals(toCompare.x, other.x) &&
                    Comparison.TolerantEquals(toCompare.y, other.y) &&
                    Comparison.TolerantEquals(toCompare.z, other.z);
        }
    }
}