﻿using System;

namespace Common.AssetBundleService
{
	public enum Persistence
	{
		None,
		Partial,
		Full,
	}
}

